import {LitElement, html} from 'lit-element';

class GlycosmosSubsumption extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      api: { type: String },
      mass: { type: String },
      range: { type: String },
      wurcs: { type: String },
      gtcid: { type: String },
      baseArray: { type: Array, reflect: true },
      limit: { type: Number },
      page: { type: Number },
      count: { type: Number, reflect: true },
      imageFile: { type: String }
    };
  }

constructor() {
  super();
  this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
  this.api = "https://test.api.glycosmos.org/"
  this.mass="";
  this.range = "";
  this.wurcs = "";
  this.gtcid = "";
  this.limit = 10;
  this.page = 1;
  this.imageFile = "https://image.beta.glycosmos.org/";
}

connectedCallback() {
  super.connectedCallback();
  if(!this.gtcid){
    this.getBaseJson();
    this.getCount();
  }else{
    this.getWURCS();
  }
}

getWURCS(){
  fetch(this.api + "glytoucan/sparql/gtcid2seqs?gtcid="+ this.gtcid).then((response) => {
    return response.json();
  }).then((myJson) => {
    this.wurcs = myJson[0]['wurcs'];
    this.getBaseJson();
    this.getCount();
  });
}

getBaseJson() {
    this.baseArray = ["Loading..."];
    this.offset = this.limit * (this.page -1);
    if(!this.wurcs){
      var url = this.domain + "glycosmos_baseComposition_list?mpz=" + this.mass + '&range='+this.range + "&limit=LIMIT%20" + this.limit + "&offset=" + this.offset;
    }else{
      var url = this.domain + "glycosmos_baseComposition_bywurcs?wurcs=" + this.wurcs + "&limit=LIMIT+10&offset=0"
    }
     
    fetch(url ).then((response) => {
      if(!response.ok){
        throw new Error();
      }
      return response.json();
    }).then((myJson) => {
      console.log(url + "\n response is OK");
      this.baseArray = myJson;
    }).catch((error)=>{
      this.baseArray = [];
    });

}

getCount(){
  this.count = 0;
  if(!this.wurcs){
    var url_2  = this.domain + "glycosmos_baseComposition_list?mpz=" + this.mass + '&range='+this.range + "&limit=&offset=0";
  }else{
    var url_2 = this.domain + "glycosmos_baseComposition_bywurcs?wurcs=" + this.wurcs + "&limit=LIMIT+10&offset=0"
  }
  fetch(url_2).then((response) => {
    return response.json();
  }).then((myJson) => {
    this.count = myJson.length;
    // console.log("count" + this.count);
  });
}

render() {
  return html `
  <style>
  .tooltips {

    max-width:80%;
    padding:4px;
    border:1px hidden;
    border-radius: 10px;

    background-color:	#EAD9FF;
    word-break:break-all;
    font-size:75%;

  }

  .wurcshits{
    border-style: solid;
    border-color: #C299FF;
  }


  </style>
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="container-fluid">
      <table class="table table-bordered table-hover table-sm" style="border-collapse: separate; border-spacing: 0;">
        <thead>
               <tr>
               <th scope="col" class="sticky-top shadow-sm bg-white text-center" style="white-space: nowrap">Mass</th>
               <th scope="col" class="sticky-top shadow-sm bg-white text-center" style=" white-space: nowrap">Base composition</th>
               <th scope="col" class="sticky-top shadow-sm bg-white text-center" style="white-space: nowrap">Subsumed composition</th>
               </tr>
           </thead>
            <tbody >
              ${this._processHtml()}
             </tbody>
             </table>
        <div class="d-flex justify-content-between mb-4">
             ${this.infoTemplate}
             ${this.pageTemplate}
       </div>
     </div>
 `;
}

_processHtml() {
   if(this.baseArray[0] == "Loading..."){
     return html`
     <tr>
      <td colspan="2">
          <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
      </td>
    <tr>
     `;
   }else if (this.baseArray.length == 0) {

     return html `<td colspan="3" class="text-center">Not found</td>`;
   }else{
     const base_img = this.api + "wurcs2image/0.8.0/png/binary/";
     console.log(this.shadowRoot.getElementById("test"));
     return html`
      ${this.baseArray.map((base_i) => html `
        <tr>
          <td class="text-center" style="width:5%;  margin:0;">${base_i.mass}</td>
          <td class="text-center" style="width:15%; ">${this.template(base_i)}</td>
          <td colspan=3 style="margin:0; padding:0;"><glycosmos-subsume imageFile="${this.imageFile}" wkey=${base_i.key} domain=${this.domain} api=${this.api} wurcs=${this.wurcs}></glycosmos-subsume></td>
        </tr>
        `)}
      `;
   }

}

template(object){
  const base_img = this.api + "wurcs2image/0.8.0/png/binary/";
  if(object.wurcs == this.wurcs){
    var div_class = "mx-auto wurcshits"
  }else{
    var div_class = "mx-auto"
  }
  return html `<div id="${object.wurcs}" class=${div_class} >
  <glytoucan-image imageFile="${this.imageFile}" wurcs=${object.key.slice(32)} imageApi=${base_img} @click="${this.handleClick}" ></glytoucan-image>
      ${(object.url == "undefined")? html`<h6>Not registered with GlyTouCan</h6>`:
      html`<h5 >GlyTouCan ID: <a href=${"https://glycosmos.org/glycans/show?gtc_id=" + object.url} >${object.url}</a></h5>`}
      </div>
      `;
}
handleClick(e){

  if(e.path[5].id.indexOf('WURCS') != -1){
    if(e.path[5].children.length == 2 ){
      var wurcs_div = document.createElement('h6');
      wurcs_div.innerHTML = e.path[5].id;
      wurcs_div.className = "mx-auto tooltips";
      e.path[5].appendChild(wurcs_div);
    }else{
      e.path[5].removeChild(e.path[5].children[2]);
    }
  }


}

get infoTemplate() {
  if (this.count) {
    return html`
      Showing ${this.offset + 1} to ${this.offset + this.limit > this.count? html`
        ${this.count}
      `:html`
        ${this.offset + this.limit}
      `} of ${this.count} entries
    `;
  } else if (this.baseArray.length == 0) {
    return html ``;
  }else {
    return html`
    <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
      <span class="sr-only">Loading...</span>
    </div>
    `;
  }
}


get pageTemplate() {
  if (this.count) {
    const maxPage = Math.ceil(this.count / this.limit);
    const pointPage = maxPage - this.page;
    return html`
    <nav aria-label="Page navigation">
      <ul class="pagination" style="margin-bottom: -1rem;">
        ${this.page == 1? html`
          <li class="page-item disabled"><button class="page-link" tabindex="-1" aria-disabled="true">Previous</button></li>
        `:html`
          <li class="page-item"><button class="page-link" @click="${() => { this.page--; this.getBaseJson(); }}">Previous</button></li>
          ${this.page < 5? html`
            ${this.beforeRepart(this.page - 1)}
          `:html`
            <li class="page-item"><button class="page-link" value="1" @click="${(e) => { this.transitionPage(e.target.value); }}}">1</button></li>
            <li class="page-item disabled"><span class="page-link">...</span></li>
            ${this.page <= maxPage - 4? html`
              ${this.beforeRepart(2)}
            `:html`
              ${this.beforeRepart(4 - pointPage)}
            `}
          `}
        `}
        <li class="page-item active" aria-current="page"><span class="page-link">${this.page}<span class="sr-only">(current)</span></span></li>
        ${this.page == maxPage? html`
          <li class="page-item disabled"><button class="page-link" tabindex="-1" aria-disabled="true">Next</button></li>
        `:html`
          ${this.page <= maxPage - 4? html`
            ${this.page < 5? html`
              ${this.afterRepart(5 - this.page)}
            `:html`
              ${this.afterRepart(2)}
            `}
            <li class="page-item disabled"><span class="page-link">...</span></li>
            <li class="page-item"><button class="page-link" value="${maxPage}" @click="${(e) => { this.transitionPage(e.target.value); }}}">${maxPage}</button></li>
          `:html`
            ${this.afterRepart(pointPage)}
          `}
          <li class="page-item"><button class="page-link" @click="${() => { this.page++; this.getBaseJson(); }}">Next</button></li>
        `}
      </ul>
    </nav>
    `;
  } else if (this.baseArray.length == 0) {
    return html ``;
  }else {
     return html`
       <div class="spinner-border" role="status">
         <span class="sr-only">Loading...</span>
       </div>
    `;
  }
}

beforeRepart(range) {
  return html`
    ${[...Array(range).keys()].reverse().map(i => ++i).map(i => html`
      <li class="page-item"><button class="page-link" value="${this.page - i}" @click="${(e) => { this.transitionPage(e.target.value); }}">${this.page - i}</button></li>
    `)}
  `;
}

afterRepart(range) {
  return html`
    ${[...Array(range).keys()].map(i => ++i).map(i => html`
      <li class="page-item"><button class="page-link" value="${this.page + i}" @click="${(e) => { this.transitionPage(e.target.value); }}}">${this.page + i}</button></li>
    `)}
  `;
}

transitionPage(pageNumber) {
  this.page = Number(pageNumber);
  this.getBaseJson();
}


}


customElements.define('glycosmos-subsumption', GlycosmosSubsumption);

