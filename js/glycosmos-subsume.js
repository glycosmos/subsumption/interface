import {LitElement, html} from 'lit-element';

class GlycosmosSubsume extends LitElement {
  static get properties() {
    return {
      api: { type: String },
      responseWArr: { type: Array },
      wkey: { type: String },
      wurcs: { type: String },
      rowspanArr: { type: Object },
      imageFile: { type: String }
    };
  }

constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.api = "https://test.api.glycosmos.org/"
    this.responseWArr = [];
    this.wkey="";
    this.wurcs="";
    this.rowspanArr={};
    this.imageFile = "https://image.beta.glycosmos.org/";
  }

connectedCallback() {
    super.connectedCallback();
    this.getSubsumeJson()
  }

getSubsumeJson(){
  this.responseWArr = ["Loading..."];
  const url = this.domain + 'glycosmos_subsumes_list?wurcs-key-uri=' +encodeURIComponent(this.wkey);
  fetch(url).then((response) => {
    return response.json();
  }).then((myJson) => {
    // console.log(url + "\n response is OK");
    this.responseWArr = myJson;



  });
}


render() {
  return html `
  <style>
  .tooltips {
        width:90%;
        padding:4px;
        border:1px hidden;
        border-radius: 10px;

        background-color:	#EAD9FF;
        word-break:break-all;
        font-size:70%;
        /* text-align: center; */
  }
  .wurcshits{
    border-style: solid;
    border-color: #C299FF;
  }

  td{
    width:33%;
  }
  </style>
  <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
  <div  style="width:100%; padding:0; margin:0;">
  <table class="table table-bordered table-hover table-sm " style="width:100%; height:100%;  padding:0; margin:0; border-collapse: separate; border-spacing: 0;">
  <thead>
    <tr>
      <th scope="col" class="shadow-sm bg-white text-center" style="white-space: nowrap;　width:33%;">Monosaccharide composition</th>
      <th scope="col" class="shadow-sm bg-white text-center" style="white-space: nowrap; width:33%;">Glycosidic topology</th>
      <th scope="col" class="shadow-sm bg-white text-center" style="white-space: nowrap; width:33%;">Linkage defined structure</th>
    </tr>
  <tbody>
    ${this._processHtml()}
  </tbody>
  </table>
</div>

 `;
}



_processHtml() {

  if (this.responseWArr[0] == "Loading..."){
    return html `<div class="spinner-border" style="width: 5rem; height: 5rem; " role="status">
      <span class="sr-only">Loading...</span>
    </div>`;
  }else if (this.responseWArr[this.wkey] == undefined) {

    return html `<td colspan="3" style="text-align:center; margin:0">Not found</td>`;
  }else{
    // calculate the number of linkage define saccharide for each monosacchiride composition
    this.link_num();
    const base_img = this.api + "wurcs2image/0.8.0/png/binary/";
    var arr = this.responseWArr;
    return html `${arr[this.wkey].map((mono) => html `
    ${(mono.type.indexOf('Monosaccharide')>-1) ? html ` <!-- if base composition subsumes monosaccharide(normal) -->
            ${(!arr[mono.key]) ? html `     
            <tr><td class="text-center">${this.template(mono, this.wkey)}</td>
            <td class="text-center"><h5>Not found</h5></td><td class="text-center"><h5>Not found</h5></td></tr>
          `:html `
            ${arr[mono.key].map((topo, i) => html `
              ${(!arr[topo.key]) ? html `
                ${(i>0) ? html `
                <tr><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>
                `:html `
                  <tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td>
                  <td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`}
              ` :html `
                  ${(i>0) ? html `
                    ${arr[topo.key].map((link, j) => html `
                      ${(j>0) ? html `
                        <tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>
                        `: html`
                        <tr><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center" >${this.template(link, topo.key)}</td></tr>
                        `}`)}
                  `:html `
                    ${arr[topo.key].map((link, j) => html `
                    ${(j>0) ? html `
                      <tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>
                      `: html`
                      <tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td>
                      <td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center " >${this.template(link, topo.key)}</td></tr>
                      `}`)}
                `}`}
        `)}`}
    `:html`   <!-- if base composition subsumes topology -->
    ${(!arr[mono.key]) ? html `
        <tr><td class="text-center"><h5>Not found</h5></td>
        <td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center"><h5>Not found</h5></td></tr>
        `:html ` <!-- if linkage defined composition is found -->
          ${arr[mono.key].map((topo, i) => html `
          ${(i>0) ? html `
            <tr><td class="text-center">${this.template(topo, mono.key)}</td></tr>
          `:html `
            <tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">Not found</td>
            <td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.key)}</td><td class="text-center">${this.template(topo, mono.key)}</td></tr>
            `}
      `)}`}
    ` }
      
              <!-- end of loop ${(!arr[mono.key])} -->
       `)}`;
     //end of retunr html
       

  }

}
// calculate the number of linkage define saccharide for each monosacchiride composition
link_num(){
  var arr = this.responseWArr;
  var row_int = 0;
  arr[this.wkey].map((mono) => {
    !arr[mono.key] ? console.log("false")
    : arr[mono.key].map((topo) =>  {
      !arr[topo.key] ? row_int += 1: row_int += arr[topo.key].length
    });

    this.rowspanArr[mono.key] = row_int;
    row_int = 0;
  }  );


}


template(object, key){
  const base_img = this.api + "wurcs2image/0.8.0/png/binary/";
  if(object.wurcs == this.wurcs){
    var div_class = "mx-auto wurcshits"
  }else{
    var div_class = "mx-auto"
  }
  return html `  <div id="${object.wurcs}" class="${div_class}"　><glytoucan-image imageFile="${this.imageFile}" wurcs=${object.key.slice(32)} imageApi=${base_img} @click="${this.handleClick}"></glytoucan-image>
     <!-- <p>${object.type}</p> -->
      ${(object.url == "undefined")? html`<h6 >Not registered with GlyTouCan</h6>`:
      html`<h6>GlyTouCan ID: <a href=${"https://glycosmos.org/glycans/show?gtc_id=" + object.url} >${object.url}</a></h6>`}
      </div>
     `;
}
handleClick(e){
  if(e.path[5].id.indexOf('WURCS') != -1){
    if(e.path[5].children.length == 2 ){
      var wurcs_div = document.createElement('p');
      wurcs_div.innerHTML = e.path[5].id;
      wurcs_div.className = "mx-auto tooltips";
      e.path[5].appendChild(wurcs_div);
    }else{
      e.path[5].removeChild(e.path[5].children[2]);
    }
  }


}



}

customElements.define('glycosmos-subsume', GlycosmosSubsume);

