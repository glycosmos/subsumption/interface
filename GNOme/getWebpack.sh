#!/bin/bash
wget https://gitlab.com/glycosmos/gtc-wc-summary/-/archive/master/gtc-wc-summary-master.zip
unzip ./gtc-wc-summary-master.zip
cd gtc-wc-summary-master
rm _config.yml package-lock.json Gemfile gtc-wc-glycoct.js
sed -i -e "s/gtc-wc-summary/$1/g" package.json
rm ./webpack/entry.js
# sed -i -e "s/gtc-wc-summary/$1/g" ./webpack/entry.js
sed -i -e "s/gtc-wc-summary/$1/g" ./public/index.html
sed -i -e "s/gtc-wc-summary/$1/g" gtc-wc-summary.js
sed -i -e "s/GtcWcSummary/$2/g" gtc-wc-summary.js
mv gtc-wc-summary.js $1.js
cd ..
rm gtc-wc-summary-master.zip
mv gtc-wc-summary-master $1
echo "please run below commands."
echo "$ cd $1 && npm install --save-dev && ./node_modules/webpack-cli/bin/cli.js"
echo "please edit $1.js"｀｀｀

#get GNOme tool data
wget --no-check-certificate https://github.com/glygen-glycan-data/GNOme/archive/master.zip
unzip ./master.zip
cd GNOme-master
rm -r data
rm -r restrictions
mv GNOme.browser.composition.json ../$1/public
mv GNOme.browser.json ../$1/public
cd JS
mv hgv2.js ../../$1/
cd ../
mv JS/ ../$1/public/
cd ../
rm master.zip

#prepare
cd $1/
wget https://gitlab.com/glycosmos/subsumption/interface/raw/master/GNOme/search.js
cd webpack/
wget https://gitlab.com/glycosmos/subsumption/interface/raw/master/GNOme/entry.js
cd ../public/
wget https://gitlab.com/glycosmos/subsumption/interface/raw/master/GNOme/GNOme.browser.html
cd ../
npm install --save-dev && ./node_modules/webpack-cli/bin/cli.js
open public/GNOme.browser.html
